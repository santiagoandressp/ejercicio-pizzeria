
const url="https://raw.githubusercontent.com/madarme/persistencia/main/pizza.json";
let padre = document.getElementById("padre-pizzas");

padre.addEventListener("click", evento =>{
    let nombreP = (evento);
    if(evento.target.nodeName === "SELECT"){
        console.log(evento.target.id)
    }
    //cargarImg(nombreP,1);
});



function cargarImg(nombreP, nro) {
    leerJSON(url).then(datos=>{

        if(nombreP.toLowerCase() !== "ninguno"){
            let urlPizzaE = (datos.pizzas.filter(pizza => pizza.sabor === nombreP))[0].url_Imagen;
            if(nro === 1){
                document.getElementById("img1").src = urlPizzaE;
                modificarCheckbox("datos1",0);
                desbloquear("img1");
            } else{
                document.getElementById("img2").src = urlPizzaE;
                modificarCheckbox("datos2",0);
                desbloquear("img2");
            }
        }else{
            if(nro === 1){
                bloquear("img1")
                modificarCheckbox("datos1", 1);
            }else{
                bloquear("img2");
                modificarCheckbox("datos2", 1);
            }
        }

    })
}

function modificarCheckbox(name, nro){

    let checkboxs = Array.from(document.getElementsByName(name));
    checkboxs.map(checkbox => {
        if(nro === 0){
            checkbox.removeAttribute("disabled")
        }else{
            checkbox.setAttribute("disabled","")
            checkbox.checked = false;
        }
    });
}

async function leerJSON(url) {
    try {
        let response = await fetch(url);
        let user = await response.json();
        return user;
    } catch(err) {
        alert(err);
    }
}

function cargarElementos() {

    for (let i = 0; i < 5; i++) {
        let section = document.createElement("section");
        section.id= `padre${i}`
        section.style = "border: 2px black solid; padding:20px; margin:60px";
        section.innerHTML = `<div class="row">
                <div class="col-6"><p>Escoja sabores para pizza 1 (puede escoger uno o dos):</p></div>
                <div class="col-3">
                
                    <select name="select" class="form-select" id=hijo_sabores${i}n1>
                    
                        <option>Ninguno</option>
                    </select>
                </div>

                <div class="col-3">
                    <select name="select" class="form-select" id=hijo_sabores${i}n2>
                        <option>Ninguno</option>
                    </select>
                </div>

                <div class="col-3"></div>
            </div>

            <div style="margin-top: 90px"></div>

            <div class="row">
                <div class="col-6">
                    <p>Ingredientes adicionales (Pizza Napolitana):</p>
                </div>

                <div class="col-3">
                    <img src="" id="img1" alt="" style="display: none">
                </div>
                <div class="col-3">
                    <img src="" id="img2" alt=""  style="display: none">
                </div>

            </div>
            <div class="row">
                <div class="col-5">
                    <form action="" id=form_checkbox_hijo${i}n-1></form>
                </div>
            </div>
            <div style="margin-top: 90px"></div>
            <div class="row">
                <div class="col-5">
                    <p>Ingredientes adicionales (Pizza Napolitana):</p>
                </div>
            </div>
            <div class="row">
                <div class="col-5">
                    <form action="" id=form_checkbox_hijo${i}n-2></form>
                </div>
            </div>`; console.log(`hijo_sabores${i}n1`);

        let padre = document.getElementById("padre-pizzas");
        padre.appendChild(section);

        let sabores = document.getElementById(`hijo_sabores${i}n1`);
        let sabores1 = document.getElementById(`hijo_sabores${i}n2`);
        let padre_adic1 = document.getElementById(`form_checkbox_hijo${i}n-1`);
        let padre_adic2 = document.getElementById(`form_checkbox_hijo${i}n-2`);

        leerJSON(url).then(datos=>{
            datos.pizzas.map((pizza) => {
                let opcion1 = document.createElement("option");
                opcion1.setAttribute("value", pizza.sabor);
                opcion1.textContent = pizza.sabor;

                let opcion2 = document.createElement("option");
                opcion2.setAttribute("value", pizza.sabor);
                opcion2.textContent = pizza.sabor;

                sabores.appendChild(opcion1);
                sabores1.appendChild(opcion2);

            })
            datos.adicional.map((adicionales,i) => {
                let adicional_op = document.createElement("label");
                adicional_op.textContent = adicionales.nombre_ingrediente;
                adicional_op.setAttribute("for", `checkbox_hijo${i+1}n-1`);
                adicional_op.innerHTML += `<input type=\"checkbox\" class=\"form_input form_checkbox  \" id=\"checkbox_hijo${i+1}\" name=\"datos1\" disabled>`;
                padre_adic1.appendChild(adicional_op);

                let adicional_op1 = document.createElement("label");
                adicional_op1.textContent = adicionales.nombre_ingrediente;
                adicional_op1.setAttribute("for", `checkbox_hijo${i+1}n-2`);
                adicional_op1.innerHTML += `<input type=\"checkbox\" class=\"form_input form_checkbox \" id=\"checkboxx_hijo${i+1}\" name=\"datos2\" disabled>`;
                padre_adic2.appendChild(adicional_op1);
            })
        });
    }
}

let bloquear = ((...ids)=>{
    // Spread operator (...) sirve para colocar funciones con parametros indefinidos,
    // donde "ids" es un arreglo de parametros
    ids.map(id => document.getElementById(id).style = "display :none");
});
let desbloquear = ((...ids)=>{
    ids.map(id => document.getElementById(id).style = "width: 150px; height: 100px; display :block");
});