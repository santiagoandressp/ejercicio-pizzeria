
const url="https://raw.githubusercontent.com/madarme/persistencia/main/pizza.json";

function crear(){
    deshabilitarForm("cantidad");
    deshabilitar("btn-crear");
    desbloquear("btn-cargar");

    let cantHijos = parseInt(document.getElementById("cantidad").value);
    let padre = document.getElementById("listas-tamanio");

    cargarListas(padre,cantHijos);
}

function cargarNuevo(){
    let padre = document.getElementById("listas-tamanio");
    padre.innerHTML ="";
    habilitarForm("cantidad");
    habilitar("btn-crear");
    bloquear("btn-cargar");
}

function cargarListas(padre,cantHijos){

    for (let i = 0; i < cantHijos; i++) {
        let hijo = document.createElement("div");
        let plural = "";
        hijo.classList.add("row");
        hijo.setAttribute("style","border: 2px solid black; width:800px ; margin: 10px 70px 10px 70px; padding:20px");

        (i!==0) ? plural=`\t\t\tTamaño de pizzas ${(i+1)}` : plural =`\t\t\tTamaño de pizza ${(i+1)}`

        hijo.innerHTML = `\t<div class="col-3">
        ${plural}
        \t\t\t</div>
        \t\t\t<div class="col-2">
        \t\t\t\t<select name="select" class="form-select" id="tamanios">
        \t\t\t\t\t<option value=""></option>
        \t\t\t\t\t<option value="pequeño" >Pequeño</option>
        \t\t\t\t\t<option value="mediano" >Mediano</option>
        \t\t\t\t\t<option value="grande">Grande</option>
        \t\t\t\t</select>
        \t\t\t</div>`;

        padre.appendChild(hijo);
    }
    padre.innerHTML+= `<div class="col-5"></div><button type="submit" class="btn btn-primary align-self-center" id="btn-cargar-opciones" onclick="cargarOpciones()" style="width: 200px; height: 50px">Cargar opciones</button> `;
}

let deshabilitarForm = (id=>{
    document.getElementById(id).setAttribute("disabled","");
})

let bloquear = ((...ids)=>{
    // Spread operator (...) sirve para colocar funciones con parametros indefinidos,
    // donde "ids" es un arreglo de parametros
    ids.map(id => document.getElementById(id).style = "display :none");
})

let deshabilitar = ((...ids)=>{
    ids.map(id => document.getElementById(id).classList.add("disabled"));
});

let habilitar = ((...ids)=>{
    ids.map(id => document.getElementById(id).classList.remove("disabled"));
});

let habilitarForm = ((id)=>{
    document.getElementById(id).removeAttribute("disabled");
})

let desbloquear = ((...ids)=>{
    ids.map(id => document.getElementById(id).style = "width: 150px; height: 100px; display :block");
})